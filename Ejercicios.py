#1. Desarrollar un programa que ingrese número y determine si es número par o impar (Python-Diagrama)
'''
numero=int(input("Ingrese un numero: "))

if numero %2==0:
    print("Es par")
else:
    print("Es inpar")
'''

#2. Escriba un programa que pida el año actual y un año cualquiera y que escriba cuántos años han
#pasado desde ese año o cuántos años faltan para llegar a ese año.

'''anioActual=int(input("Ingrese el año actual: "))
anioX=int(input("Ingrese el año deseado: "))

if anioActual>anioX:
    print("La diferencia de años es de: ", anioActual-anioX)
else:
    print("La diferencia de años es de: ",anioX-anioActual)
'''

#3. Cree un programa que pida al usuario su edad y muestre por pantalla la etapa en la que se encuentre.
#a. 0 a 10 niño(@)
#b. 11 a 18 adolescente
#c. 19 a 64 adulto
#d. 65 en adelante Adulto Mayor

'''
edad=int(input("Ingrese su edad: "))

if edad >0 and edad<11:
    print("Es un niño")
elif edad >11 and edad<19:
    print("Es un adolescente")
elif edad >19 and edad<65:
    print("Es un adulto")
elif edad >65:
    print("Es un adulto mayor")
else:
    print("Ingrese una edad valida")
'''

#4. Imprima los números del 1 al 20 usando un ciclo while
'''
num=0
while num<21:
    print(num)
    num=num+1
'''

#5. Imprima los números del 20 al 1 usando un ciclo for
'''
for num in range(0,21):
    print(num)
'''

#6. Escriba un programa de alquiler de vehículos que le permita al usuario seleccionar una categoría y
#un modelo de vehículo por una cantidad de días e imprima el monto a pagar a la agencia, el sistema
#debe identificar si el cliente es frecuente o no para aplicarle un descuento y debe cobrar una póliza
#de seguro, además si el cliente requiere el vehículo más tiempo del establecido el costo por día tendrá
#un incremento dependiendo del tipo de vehículo, tenga en cuenta los siguientes parámetros:


Vehiculo=str(input("Seleccione si desea un Automovil, Hatchback o Doble Taraccion: "))
Modelo=int(input("Seleccione el modelo del vehiculo: "))
Dias=int(input("Igrese la cantidad de dias que desea:"))

if Vehiculo=="Automovil" and Modelo>2014:
    if Dias <8:
